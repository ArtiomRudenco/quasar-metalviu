
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/about', component: () => import('pages/About.vue') },
      { path: '/services', component: () => import('pages/Services.vue') },
      { path: '/contact', component: () => import('pages/Contact.vue') },
    ]
  },
  {
    path: '/category',
    component: () => import('layouts/CategoryLayout.vue'),
    children: [
      { path: '/category/gates', component: () => import('pages/categories/Gates.vue') },
      { path: '/category/fences', component: () => import('pages/categories/Fences.vue') },
      { path: '/category/railings', component: () => import('pages/categories/Railings.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
