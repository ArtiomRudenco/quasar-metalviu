// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Eroare',
  success: 'Succes',
  Main: 'Generala',
  About: 'Despre noi',
  Services: 'Servicii',
  Contact: 'Contacte',
  Gates: 'Porti',
  Fences: 'Garduri',
  Railings: 'Balustrade',
  Details: 'Detalii',
  'Modern, Classic, Loft': 'Modern, Clasic, Loft',

}
