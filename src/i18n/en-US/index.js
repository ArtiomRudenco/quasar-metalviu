// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  Main: 'Main',
  About: 'About',
  Services: 'Services',
  Contact: 'Contact',
  Gates: 'Gates',
  Fences: 'Fences',
  Railings: 'Railings',
  Details: 'Details',
  'Modern, Classic, Loft': 'Modern, Classic, Loft',
}
