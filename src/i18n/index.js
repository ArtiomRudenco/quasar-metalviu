import ruRU from './ru-RU';
import roRO from './ro-RO';
import enUS from './en-US'

export default {
  'ru-RU': ruRU,
  'ro-RO': roRO,
  'en-US': enUS,
}
