// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Ошибка',
  success: 'Успешно',
  Main: 'Главная',
  About: 'О нас',
  Services: 'Услуги',
  Contact: 'Контакты',
  Gates: 'Ворота',
  Fences: 'Заборы',
  Railings: 'Перила',
  Details: 'Описание',
  'Modern, Classic, Loft': 'Модерн, Классик, Лофт',

}
